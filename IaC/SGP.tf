#----------------------------------------
# Creating Security group
#----------------------------------------
# Sec group for "Database VPC"
module "sgp_db" {
  source              = "terraform-aws-modules/security-group/aws"
  version             = "4.16.0"
  name                = var.scg_name[0]
  vpc_id              = module.cloud_vpc.vpc_id
  ingress_cidr_blocks = lookup(var.ingress_cidr_block, "db")
  ingress_rules       = lookup(var.ingress_rule, "db")
  egress_rules        = lookup(var.egress_rule, "db")
  tags                = var.common_tags
  depends_on          = [module.cloud_vpc]
}
# Sec group for "Backend VPC"
module "sgp_backend" {
  source              = "terraform-aws-modules/security-group/aws"
  version             = "4.16.0"
  name                = var.scg_name[1]
  vpc_id              = module.cloud_vpc.vpc_id
  ingress_cidr_blocks = lookup(var.ingress_cidr_block, "backend")
  ingress_rules       = lookup(var.ingress_rule, "backend")
  egress_rules        = lookup(var.egress_rule, "backend")
  tags                = var.common_tags
  depends_on          = [module.cloud_vpc]
}
# Sec group for "Bastion VPC"
module "sgp_bastion" {
  source              = "terraform-aws-modules/security-group/aws"
  version             = "4.16.0"
  name                = var.scg_name[2]
  vpc_id              = module.bastion_vpc.vpc_id
  ingress_cidr_blocks = lookup(var.ingress_cidr_block, "bastion")
  ingress_rules       = lookup(var.ingress_rule, "bastion")
  egress_rules        = lookup(var.egress_rule, "bastion")
  tags                = var.common_tags
  depends_on          = [module.bastion_vpc]
}
# Sec group for Load-balancer
module "sgp_lb" {
  source              = "terraform-aws-modules/security-group/aws"
  version             = "4.16.0"
  name                = var.scg_name[3]
  vpc_id              = module.cloud_vpc.vpc_id
  ingress_cidr_blocks = lookup(var.ingress_cidr_block, "lb")
  ingress_rules       = lookup(var.ingress_rule, "lb")
  egress_rules        = lookup(var.egress_rule, "lb")
  tags                = var.common_tags
}