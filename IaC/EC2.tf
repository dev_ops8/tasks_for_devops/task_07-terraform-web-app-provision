#-----------------------------------------
# Creating EC-2 instances
#-----------------------------------------
# Database VM
module "db-vm" {
  source                 = "terraform-aws-modules/ec2-instance/aws"
  version                = "4.2.0"
  name                   = var.vm_name[0]
  ami                    = data.aws_ami.linux.id
  instance_type          = var.inst_type
  availability_zone      = data.aws_availability_zones.az.names[0]
  vpc_security_group_ids = tolist([module.sgp_db.security_group_id])
  subnet_id              = module.cloud_vpc.private_subnets[2]
  key_name               = "deployer-key"
  depends_on             = [module.cloud_vpc, module.sgp_db, aws_nat_gateway.nat_1]
}
# Bastion VM
module "bastion-vm" {
  source                 = "terraform-aws-modules/ec2-instance/aws"
  version                = "4.2.0"
  name                   = var.vm_name[1]
  ami                    = data.aws_ami.linux.id
  instance_type          = var.inst_type
  availability_zone      = data.aws_availability_zones.az.names[0]
  vpc_security_group_ids = tolist([module.sgp_bastion.security_group_id])
  subnet_id              = module.bastion_vpc.public_subnets[0]
  key_name               = "deployer-key"
  depends_on             = [module.bastion_vpc, module.sgp_bastion]
}
# Creation SSH-key
resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = file("~/.ssh/id_rsa.pub")
}
