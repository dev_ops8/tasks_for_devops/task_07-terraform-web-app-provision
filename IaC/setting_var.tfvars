ingress_cidr_block = {
  "db"      = ["0.0.0.0/0"]
  "backend" = ["0.0.0.0/0"]
  "lb"      = ["0.0.0.0/0"]
  "bastion" = ["0.0.0.0/0"]
}

ingress_rule = {
  "db"      = ["all-all"]
  "backend" = ["all-all"]
  "lb"      = ["http-80-tcp", "all-icmp"]
  "bastion" = ["all-all"]
}

egress_rule = {
  "db"      = ["all-all"]
  "backend" = ["all-all"]
  "lb"      = ["http-80-tcp"]
  "bastion" = ["all-all"]
}

ASG_health_check_type = "EC2"

ASG_lb_false = "false"