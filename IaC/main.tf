#------------------------------------------------
# Steps of provision highly available web-app with db, backend:
#   - VPC(subnet) for all instances
#   - VPC peering beetween instances
#   - Nat gateway's
#   - Security group for VPC's
#   - Launch config for our instances
#   - Load-balancer in 2 AZ
#   - Auto-scaling group of backend in 2 AZ
#   - Bastion EC2 for connecting to private subnet's
#   - For first we must exec terraform with command terraform apply -var-file=setting_var.tfvars
#   - Step above give us possibility to provide via ansible our setting's
#   - After setting we must exec terraform apply to get the setting's for normal work mode
#-----------------------------------------------
provider "aws" {
  region                  = var.region
  shared_credentials_file = "~/.aws/credentials"
}
# Collecting data
#-----------------------------------
data "aws_availability_zones" "az" {}
data "aws_ami" "linux" {
  owners      = ["099720109477"]
  most_recent = true
  filter {
    name   = "name"
    values = [var.image]
  }
}
