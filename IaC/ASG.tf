#----------------------------------------
# Creating ASG and Auto-launch config
#----------------------------------------
# Backend VM - Auto-launch config
resource "aws_launch_configuration" "backend" {
  name_prefix     = "lc-backend-"
  image_id        = data.aws_ami.linux.id
  instance_type   = var.inst_type
  security_groups = [module.sgp_backend.security_group_id]
  key_name        = "deployer-key"
  lifecycle {
    create_before_destroy = true
  }
  depends_on = [module.sgp_backend]
}
# Auto-scaling group for backend
resource "aws_autoscaling_group" "backend" {
  name_prefix          = "ASG-backend-"
  launch_configuration = aws_launch_configuration.backend.name
  min_size             = 2
  max_size             = 4
  min_elb_capacity     = 2
  health_check_type    = var.ASG_health_check_type == "ELB" ? "ELB" : "EC2"
  health_check_grace_period = 60
  enabled_metrics = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupTotalInstances"
  ]
  metrics_granularity = "1Minute"
  vpc_zone_identifier = [module.cloud_vpc.private_subnets[0], module.cloud_vpc.private_subnets[1]]
  load_balancers      = var.ASG_lb_false == "lb" ? [aws_elb.web.name] : []
  dynamic "tag" {
    for_each = {
      Name = "ASG-backend"
    }
    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }
  lifecycle {
    create_before_destroy = true
  }
  depends_on = [
    aws_launch_configuration.backend,
    aws_elb.web, module.db-vm,
    aws_nat_gateway.nat_1,
    aws_nat_gateway.nat_2,
  ]
}