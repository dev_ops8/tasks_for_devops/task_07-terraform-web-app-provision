terraform {
  required_providers {
    aws = {
      version = "4.20.0"

    }
  }
  required_version = ">=0.13"
}