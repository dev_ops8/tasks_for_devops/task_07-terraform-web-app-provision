output "bastion_vm_public_ip" {
  description = "public_ip"
  value       = module.bastion-vm.public_ip
}

output "load_balancers-dns_name" {
  description = "DNS name of LB"
  value       = aws_elb.web.dns_name
}