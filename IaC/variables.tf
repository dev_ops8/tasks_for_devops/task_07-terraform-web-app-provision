variable "region" {
  type    = string
  default = "eu-central-1"
}

variable "vpc_name" {
  type    = list(string)
  default = ["bastion_vpc", "cloud_vpc"]
}

variable "cidr_range" {
  type    = list(string)
  default = ["10.1.0.0/16", "10.2.0.0/16"]
}

variable "vpc_private_subnets" {
  type = map(any)
  default = {
    "cloud" = ["10.2.1.0/24", "10.2.2.0/24", "10.2.3.0/24"]
  }
}

variable "vpc_public_subnets" {
  type = map(any)
  default = {
    "cloud"   = ["10.2.11.0/24", "10.2.12.0/24"]
    "bastion" = ["10.1.1.0/24"]
  }
}

variable "scg_name" {
  type = list(string)
  default = [
    "Security group for Database VPC",
    "Security group for Backend VPC",
    "Security group for Bastion VPC",
    "Security group for Load-balancer"
  ]
}

variable "ingress_cidr_block" {
  type = map(any)
  default = {
    "db"      = ["10.2.1.0/24", "10.2.2.0/24"]
    "backend" = ["10.1.1.0/24", "10.2.3.0/24", "10.2.11.0/24", "10.2.12.0/24"]
    "lb"      = ["0.0.0.0/0"]
    "bastion" = ["0.0.0.0/0"]
  }
}

variable "ingress_rule" {
  type = map(any)
  default = {
    "db"      = ["mysql-tcp"]
    "backend" = ["https-443-tcp", "http-80-tcp", "mysql-tcp", "ssh-tcp"]
    "lb"      = ["http-80-tcp"]
    "bastion" = ["ssh-tcp"]
  }
}

variable "egress_rule" {
  type = map(any)
  default = {
    "db"      = ["mysql-tcp"]
    "backend" = ["https-443-tcp", "http-80-tcp", "mysql-tcp", "ssh-tcp"]
    "lb"      = ["http-80-tcp"]
    "bastion" = ["ssh-tcp"]
  }
}

variable "vm_name" {
  type    = list(string)
  default = ["db_vm", "Bastion_vm"]
}

variable "image" {
  type    = string
  default = "ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"
}

variable "inst_type" {
  type    = string
  default = "t2.micro"
}

variable "ASG_health_check_type" {
  type    = string
  default = "ELB"
}

variable "ASG_lb_false" {
  type    = string
  default = "lb"
}

variable "common_tags" {
  type = map(string)
  default = {
    Owner       = "admin"
    Environment = "dev"
  }
}

variable "nat_1" {
  type = map(string)
  default = {
    Name = "NAT_1"
  }
}

variable "nat_2" {
  type = map(string)
  default = {
    Name = "NAT_2"
  }
}