#-------------------------------------------
# Creating load-balancer 
#------------------------------------------
resource "aws_elb" "web" {
  name                      = "web-server"
  security_groups           = [module.sgp_lb.security_group_id]
  subnets                   = module.cloud_vpc.public_subnets[*]
  cross_zone_load_balancing = true
  listener {
    lb_port           = 80
    lb_protocol       = "http"
    instance_port     = 80
    instance_protocol = "http"
  }
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 5
    target              = "HTTP:80/index.html"
    interval            = 10
  }
  depends_on = [module.sgp_lb]
}