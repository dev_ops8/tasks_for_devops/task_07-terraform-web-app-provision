#------------------------------------
# Route's beetwen VPC's
#------------------------------------
# Route from Cloud to Bastion
resource "aws_route" "cloud_to_bastion" {
  count                     = length(module.cloud_vpc.private_route_table_ids)
  route_table_id            = element(module.cloud_vpc.private_route_table_ids, count.index)
  destination_cidr_block    = var.cidr_range[0]
  vpc_peering_connection_id = aws_vpc_peering_connection.bastion-cloud.id
  depends_on                = [aws_vpc_peering_connection.bastion-cloud, module.cloud_vpc, module.bastion_vpc]
}
# Route from Bastion to Cloud
resource "aws_route" "bastion_to_cloud" {
  count                     = length(module.bastion_vpc.public_route_table_ids)
  route_table_id            = element(module.bastion_vpc.public_route_table_ids, count.index)
  destination_cidr_block    = var.cidr_range[1]
  vpc_peering_connection_id = aws_vpc_peering_connection.bastion-cloud.id
  depends_on                = [aws_vpc_peering_connection.bastion-cloud, module.cloud_vpc, module.bastion_vpc]
}
#----------------------------------
# Route's to Web via NAT-gateway
#----------------------------------
# Route from private subnet back_1 to web
resource "aws_route" "back-1_to_web" {
  route_table_id         = module.cloud_vpc.private_route_table_ids[0]
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_nat_gateway.nat_1.id
  depends_on             = [aws_nat_gateway.nat_1]
}
# Route from private subnet Back-2 to web
resource "aws_route" "back-2_to_web" {
  route_table_id         = module.cloud_vpc.private_route_table_ids[1]
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_nat_gateway.nat_2.id
  depends_on             = [aws_nat_gateway.nat_2]
}
# Route from private subnet DB to web
resource "aws_route" "db_to_web" {
  route_table_id         = module.cloud_vpc.private_route_table_ids[2]
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_nat_gateway.nat_1.id
  depends_on             = [aws_nat_gateway.nat_1]
}