#-----------------------------------
# Creating VPC's and subnet's
#----------------------------------
# Bastion VPC
module "bastion_vpc" {
  source         = "terraform-aws-modules/vpc/aws"
  version        = "3.18.0"
  name           = var.vpc_name[0]
  cidr           = var.cidr_range[0]
  azs            = [data.aws_availability_zones.az.names[0]]
  public_subnets = lookup(var.vpc_public_subnets, "bastion")
  tags           = var.common_tags
}
# Cloud VPC
module "cloud_vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.18.0"
  name    = var.vpc_name[1]
  cidr    = var.cidr_range[1]
  azs = [
    data.aws_availability_zones.az.names[0],
    data.aws_availability_zones.az.names[1],
    data.aws_availability_zones.az.names[0]
  ]
  private_subnets = lookup(var.vpc_private_subnets, "cloud")
  public_subnets  = lookup(var.vpc_public_subnets, "cloud")
  tags            = var.common_tags
}
#------------------------------------
# Creating NAT-gateway's for our private subnet's
#------------------------------------
# Create EIP's
resource "aws_eip" "elastic_ip_1" {
  vpc  = true
  tags = var.nat_1
}
resource "aws_eip" "elastic_ip_2" {
  vpc  = true
  tags = var.nat_2
}
# Create NAT-gateway for subnet cloud_public[0]
resource "aws_nat_gateway" "nat_1" {
  allocation_id = aws_eip.elastic_ip_1.id
  subnet_id     = module.cloud_vpc.public_subnets[0]
  tags          = var.nat_1
  depends_on    = [module.cloud_vpc.igw_id]
}
# Create NAT-gateway for subnet cloud_public[1]
resource "aws_nat_gateway" "nat_2" {
  allocation_id = aws_eip.elastic_ip_2.id
  subnet_id     = module.cloud_vpc.public_subnets[1]
  tags          = var.nat_2
  depends_on    = [module.cloud_vpc.igw_id]
}
#------------------------------------
# VPC-peering
#------------------------------------
#### Creating VPC-peering bastion-cloud
resource "aws_vpc_peering_connection" "bastion-cloud" {
  peer_vpc_id = module.cloud_vpc.vpc_id
  vpc_id      = module.bastion_vpc.vpc_id
  auto_accept = true
  tags        = var.common_tags
}
