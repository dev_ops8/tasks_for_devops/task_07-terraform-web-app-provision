# task_07-terraform-web-app-provision

## Description of the task
Deploy web-app from task_06 to cloud use methods of IaC. Web-app must have DB, auto-scaling group, load-balancer. Infrastructure must has zero DownTime and blue-green deployment.

## Check-points:
- Create infrastructure use methodology of IaC
- Deploy web-app from task_06 using ansible
- Try to automate all steps as possible as you can

## Result:
- Create next global diagram of our infrastructure
- For first step we must run "terraform apply -var-file=setting_var.tfvars"
- Step above, allow to install some package's via ansible from internet(it's possible cause we change ingress and egress rule's to allow all traffic from/to internet)
- After deploy playbook via ansible, we must change ingress and egress rule's to protect our infra from outside
- Also we can do snapshoot our db, backend server's for next usability like an ami image in our infra 
- Run "terraform apply" - to set right ingress and egress rule's
- As a result we created infra via terraform provisioning. Also we can access on Bastion and manage our backend's instance's via ssh

P.S. Instead of EC2 database(mysql in our case) we can use RDS service of Amazon for preference. 

<p align="center">
  <img src="./diagram.jpg" />
</p>